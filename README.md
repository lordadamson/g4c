# g4c
A graphics library for C++ by Dr. Mohammed Samy.

This project was once popular among the students of FCIS ASU. But was long forgotten when Dr Samy mysteriously disappeared from the internet.

This is my attempt to revive Dr. Samy's great projects.

## Usage:
You can see the documentation that I managed to acquire in [here](http://lordadamson.github.com/g4c).

Or you can watch Mohammed Osama's [Videos](https://www.youtube.com/watch?v=OSkn9JHB2o8&list=PLdBzO5n0Xi9HcF2k-RhFBsuxRD6ksrxOX).
